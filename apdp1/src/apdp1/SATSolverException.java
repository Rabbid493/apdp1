package apdp1;

/**
 * 
 * @author Jaime Ruiz-Borau Vizarraga (NIP: 546751)
 *
 *         Clase para excepciones personalizadas de SATSolver
 */

public class SATSolverException extends Exception {

	/**
	 * Default serial version UID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 */
	public SATSolverException() {
		super();
	}

	/**
	 * Constructor
	 * 
	 * @param message
	 *            : Mensaje de error a mostrar
	 */
	public SATSolverException(String message) {
		super(message);
	}
}
