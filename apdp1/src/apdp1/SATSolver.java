package apdp1;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * 
 * @author Jaime Ruiz-Borau Vizarraga (NIP: 546751)
 *
 *         Esta clase implementa el funcionamiento principal del programa
 *         SATSolver, y es desde el cual se realizan todas las llamadas al resto
 *         de metodos de las demas clases
 */

public class SATSolver {

	/* Atributos privados */

	// FormulaCNF cargada. Si es null no hay ninguna cargada
	private static FormulaCNF f;

	// Scanner global del que leer la entrada del usuario
	private static Scanner s;

	public static void main(String[] args) {
		f = null;
		/* Si no hay argumentos, mostramos menu */
		if (args.length == 0) {
			s = new Scanner(System.in);
			saludo();
			menu();
			int opcion = solicitarOpcion();
			while (procesarOpcion(opcion)) {
				menu();
				opcion = solicitarOpcion();
			}
			s.close();
		} else {
			if (args[0].equals("demo")) {
				System.out.println("Bienvenido a la demo de SATSolver. A continuacion se generaran y "
						+ "resolveran 3 formulas de cada tipo de 10 literales y 10 clausulas");
				procesarInfoRandom(3, 10, 10, 0);
				procesarInfoRandom(3, 10, 10, 1);
				procesarInfoRandom(3, 10, 10, 2);
			} else if (args[0].equals("randomTimes")) {
				try {
					int f = Integer.parseInt(args[1]);
					int c = Integer.parseInt(args[2]);
					int l = Integer.parseInt(args[3]);
					int t = Integer.parseInt(args[4]);

					procesarInfoRandomTimes(f, c, l, t);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("Uso incorrecto del programa. Revise los parametros");
					help();
				}
			} else {
				System.out.println("Uso incorrecto del programa");
				help();
			}
		}
	}

	/**
	 * Imprime por pantalla un saludo
	 */
	private static void saludo() {
		System.out.println("=-=-=-=-=-=-=-=-=-=-=-=");
		System.out.println("Bienvenido a SAT Solver");
		System.out.println("=-=-=-=-=-=-=-=-=-=-=-=");
		if (f != null) {
			System.out.println("Formula CNF cargada");
		} else {
			System.out.println("No hay formula CNF cargada");
		}
		System.out.println();
	}

	/**
	 * Imprime por pantalla la ayuda
	 */
	private static void help() {
		System.out.println("Usos posibles del programa SATSolver:");
		System.out.println("\tSin argumentos: Carga el menu inicial del programa");
		System.out.println(
				"\tSATSolver demo: Genera y resuelve 3 formulas aleatorias de cada tipo, de 10 literales y 10 clausulas");
		System.out.println(
				"\tSATSolver randomTimes f c l t: Genera 'f' formulas aleatorias, de 'c' numero de clausulas y con 'l' numero de literales");
		System.out
				.println("\t\t\t\t\tEl entero 't' indica el tipo de formulas: 0 = 2SAT, 1 = HornSAT, 2 = SAT generico");
	}

	/**
	 * Imprime por pantalla un menu
	 */
	private static void menu() {
		System.out.println("====");
		System.out.println("Menu");
		System.out.println("====");
		if (f != null) {
			System.out.println("1. Introducir otra formula CNF a mano");
			System.out.println("2. Introducir nombre de fichero con otra formula CNF");
			System.out.println("3. Resolver formulas SAT generadas aleatoriamente");
			System.out.println("4. Resolver formula CNF cargada");
			System.out.println("5. Indicar tipo de formula CNF cargada");
			System.out.println("6. Salir");
			System.out.println("'help' muestra ayuda de como ejecutar SATSolver");
		} else {
			System.out.println("1. Introducir formula CNF a mano");
			System.out.println("2. Introducir nombre de fichero con formula CNF");
			System.out.println("3. Resolver formulas SAT generadas aleatoriamente");
			System.out.println("4. Salir");
			System.out.println("'help' muestra ayuda de como ejecutar SATSolver");
		}
	}

	/**
	 * Solicita al usuario que introduzca una opcion del menu
	 * 
	 * @return entero con la opcion introducida
	 */
	private static int solicitarOpcion() {
		System.out.println("Introduzca eleccion: ");
		String respuesta = s.nextLine();
		if (respuesta != null && respuesta.equals("help")) {
			help();
			return 0;
		}
		try {
			int respuestaInt = Integer.parseInt(respuesta);
			if (f != null) {
				if (respuestaInt < 1 || respuestaInt > 6) {
					s.close();
					throw new SATSolverException();
				}
			} else {
				if (respuestaInt < 1 || respuestaInt > 4) {
					s.close();
					throw new SATSolverException();
				}
			}
			return respuestaInt;
		} catch (SATSolverException e) {
			System.out.println("Respuesta no valida, intentelo de nuevo");
			menu();
			return solicitarOpcion();
		}
	}

	/**
	 * Procesa la opcion del usuario
	 * 
	 * @param opcion
	 *            : entero que representa la opcion del usuario
	 * 
	 * @return true si tenemos que seguir interaccionando con el usuario. false
	 *         para finalizar
	 */
	private static boolean procesarOpcion(int opcion) {
		if (opcion == 0) {
			return true;
		} else if (opcion == 1) {
			solicitarFormulaCNF();
			return true;
		} else if (opcion == 2) {
			solicitarFicheroCNF();
			return true;
		} else if (opcion == 3) {
			try {
				solicitarInfoRandom();
			} catch (SATSolverException e) {
				System.out.println(e.getMessage());
			} catch (NumberFormatException e) {
				System.out.println("No es un numero valido. Vuelva a intentarlo");
			}
			return true;
		} else if (opcion == 4) {
			if (f != null) {
				resolverFormula();
				return true;
			} else {
				return false;
			}
		} else if (opcion == 5) {
			if (f != null) {
				mostrarTipoFormula();
				return true;
			} else {
				return true;
			}
		} else if (opcion == 6) {
			if (f != null) {
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	/**
	 * Solicita al usuario por teclado informacion para generar aleatoriamente
	 * formulas CNF
	 */
	private static void solicitarInfoRandom() throws SATSolverException, NumberFormatException {
		System.out.println("Introduzca el numero de formulas aleatorias a generar");
		String numForms = s.nextLine();
		int iNumForms = Integer.parseInt(numForms);
		if (iNumForms < 1) {
			throw new SATSolverException("Numero invalido de formulas");
		}

		System.out.println(
				"Introduzca el numero de literales en total que deberan poseer las formulas (0 o negativo para aleatorio)");
		String numLits = s.nextLine();
		int iNumLits = Integer.parseInt(numLits);

		System.out.println(
				"Introduzca el numero de clausulas en total que deberan poseer las formulas (0 o negativo para aleatorio)");
		String numClaus = s.nextLine();
		int iNumClaus = Integer.parseInt(numClaus);

		System.out.println(
				"Introduzca el tipo de las formulas. 0 = 2SAT, 1 = HornSAT, resto de enteros positivos = SAT normal (negativo para aleatorio)");
		String tipo = s.nextLine();
		int iTipo = Integer.parseInt(tipo);

		procesarInfoRandom(iNumForms, iNumClaus, iNumLits, iTipo);
	}

	/**
	 * Genera y resuelve formulas CNF aleatoriamente, mostrando su solucion por
	 * pantalla
	 * 
	 * @param iNumForms
	 *            : numero de formulas
	 * @param iNumClaus
	 *            : numero de clausulas
	 * @param iNumLits
	 *            :numero de literales
	 * @param iTipo
	 *            : tipo de formulas formadas. 0 = 2SAT, 1 = HornSAT, otro
	 *            numero = SAT
	 */
	private static void procesarInfoRandom(int iNumForms, int iNumClaus, int iNumLits, int iTipo) {
		Random r = new Random();
		int c = iNumClaus;
		int l = iNumLits;
		int t = iTipo;
		for (int i = 0; i < iNumForms; i++) {
			if (iNumClaus <= 0) {
				c = r.nextInt(200) + 1;
			}

			if (iNumLits <= 0) {
				l = r.nextInt(200) + 1;
			}

			if (iTipo < 0) {
				t = r.nextInt(3);
			}
			f = randomFormula(c, l, t);
			mostrarTipoFormula();
			resolverFormula();
			System.out.println();
		}
	}

	/**
	 * Genera y resuelve formulas CNF aleatoriamente, mostrando informacion
	 * estadistica por pantalla
	 * 
	 * @param iNumForms
	 *            : numero de formulas
	 * @param iNumClaus
	 *            : numero de clausulas
	 * @param iNumLits
	 *            :numero de literales
	 * @param iTipo
	 *            : tipo de formulas formadas. 0 = 2SAT, 1 = HornSAT, otro
	 *            numero = SAT
	 */
	private static void procesarInfoRandomTimes(int iNumForms, int iNumClaus, int iNumLits, int iTipo) {
		Random r = new Random();
		int c = iNumClaus;
		int l = iNumLits;
		int t = iTipo;
		boolean[] prints = new boolean[3];
		for (int i = 0; i < prints.length; i++) {
			prints[i] = false;
		}

		System.out.println("Resolviendo " + iNumForms + " formulas de tipo " + getTipoSAT(iTipo) + " con " + iNumClaus
				+ " clausulas de " + iNumLits + " literales");
		double media = 0;
		System.out.print("0%  ");
		for (int i = 0; i < iNumForms; i++) {
			if (((i * 100) / iNumForms) >= 25 && !prints[0]) {
				prints[0] = true;
				System.out.print("  25%  ");
			}
			if (((i * 100) / iNumForms) >= 50 && !prints[1]) {
				prints[1] = true;
				System.out.print("  50%  ");
			}
			if (((i * 100) / iNumForms) >= 75 && !prints[2]) {
				prints[2] = true;
				System.out.print("  75%  ");
			}
			if (iNumClaus <= 0) {
				c = r.nextInt(200) + 1;
			}

			if (iNumLits <= 0) {
				l = r.nextInt(200) + 1;
			}

			if (iTipo < 0) {
				t = r.nextInt(3);
			}
			f = randomFormula(c, l, t);
			long startTime = System.nanoTime();
			resolverFormulaSilent();
			long endTime = System.nanoTime();
			long duration = (endTime - startTime) / (long) (1000000.0);

			media = media + duration;
		}
		System.out.println("100%");
		media = media / ((double) iNumForms);
		DecimalFormat df = new DecimalFormat("#.##");
		df.setRoundingMode(RoundingMode.CEILING);
		System.out.println(df.format(media) + "ms de media de resolucion de " + iNumForms + " formulas de tipo "
				+ getTipoSAT(iTipo) + " con " + iNumClaus + " clausulas de " + iNumLits + " literales");
		System.out.println();
	}

	/**
	 * Dado un entero devuelve un String con el tipo de SAT
	 * 
	 * @param tipo
	 *            : entero de entrada
	 * 
	 * @return si tipo = 0, devuelve "2SAT". Si tipo = 1, devuelve "HornSAT".
	 *         Para cualquier otro caso, devuelve "SAT"
	 */
	private static String getTipoSAT(int tipo) {
		if (tipo == 0) {
			return "2SAT";
		} else if (tipo == 1) {
			return "HornSAT";
		} else {
			return "SAT";
		}
	}

	/**
	 * Solicita al usuario por teclado la formula CNF
	 */
	private static void solicitarFormulaCNF() {
		System.out.println("Introduzca la formula CNF con el formato especificado por el guion de la practica");
		String formula = s.nextLine();
		procesarFormula(formula);
	}

	/**
	 * Solicita al usuario por teclado el nombre del fichero con la formula CNF
	 */
	private static void solicitarFicheroCNF() {
		System.out.println("Introduzca el nombre del fichero:");
		String fichero = s.nextLine();
		procesarFichero(fichero);

	}

	/**
	 * Procesa el fichero con nombre "fichero" para obtener de el la formula CNF
	 * 
	 * @param fichero
	 *            : String con el nombre del fichero
	 */
	private static void procesarFichero(String fichero) {
		try {
			File file = new File(fichero);
			Scanner t = new Scanner(file);
			String formula = "";
			while (t.hasNextLine()) {
				formula = formula + t.nextLine();
			}
			procesarFormula(formula);
			t.close();
		} catch (FileNotFoundException e) {
			System.out.println("No se ha encontrado el fichero con el nombre " + fichero);
		}
	}

	/**
	 * Procesa la formula en formato String para convertirla en FormulaCNF
	 * 
	 * @param formula
	 *            : String con la formula escrita
	 */
	private static void procesarFormula(String formula) {
		try {
			FormulaCNF definitiva = new FormulaCNF();

			// Obtener las clausulas
			formula = formula.trim();
			String[] clausulas = formula.split("[*]");

			for (String strong : clausulas) {

				// Obtener los literales
				strong = strong.replace("(", "");
				strong = strong.replace(")", "");
				String[] literales = strong.split("[+]");
				Clausula c = new Clausula();

				for (String lit : literales) {
					Literal l = procesarLiteral(lit);
					c.addLiteral(l);
				}

				definitiva.addClausula(c);

			}

			f = definitiva;
		} catch (SATSolverException e) {
			e.printStackTrace();
			System.out.println("El formato de la formula no es el correcto.");
		}
	}

	/**
	 * Metodo auxiliar para procesar un Literal de una formula escrita
	 * 
	 * @param lit
	 *            : String con el literal por escrito
	 * 
	 * @return objeto de tipo Literal basado en el String
	 * 
	 * @throws SATSolverException
	 *             Si el formato del String del literal es invalido
	 */
	private static Literal procesarLiteral(String lit) throws SATSolverException {
		boolean negated = true;
		if (lit.charAt(0) == '-') {
			negated = false;
			lit = lit.substring(1);
		}

		if (lit.matches("[a-zA-Z\\_][a-zA-Z0-9\\_]*")) {
			return new Literal(lit, negated);
		} else {
			throw new SATSolverException("El literal no tiene el formato correcto");
		}
	}

	/**
	 * Metodo que resuelve la FormulaCNF de esta clase
	 */
	private static void resolverFormula() {
		if (f != null) {
			System.out.println("Resolviendo formula");
			System.out.println(f.toString());
			if (f.is2SAT()) {
				try {
					ArrayList<Literal> n = Algoritmos.LimitedBacktracking(f, new ArrayList<Literal>());
					if (n == null) {
						throw new SATSolverException();
					}
					for (Literal lit : n) {
						System.out.println(lit.getL() + " = " + lit.getAsignacion());
					}
					System.out.println("Formula resuelta");
					System.out.println("El valor booleano final de esta formula es " + f.applyValuesWithNull(n)
							+ ", porque es satisfactible");
				} catch (SATSolverException e) {
					System.out.println("Esta formula no es satisfactible");
				}
			} else if (f.isHornSAT()) {
				try {
					ArrayList<Literal> n = Algoritmos.Unit(f);
					if (n == null) {
						throw new SATSolverException();
					}
					for (Literal lit : n) {
						System.out.println(lit.getL() + " = " + lit.getAsignacion());
					}
					System.out.println("Formula resuelta");
					System.out.println("El valor booleano final de esta formula es " + f.applyValuesWithNull(n)
							+ ", porque es satisfactible");
				} catch (SATSolverException e) {
					System.out.println("Esta formula no es satisfactible");
				}
			} else {
				try {
					ArrayList<Literal> n = Algoritmos.DPLL(f, new ArrayList<Literal>());
					if (n == null) {
						throw new SATSolverException();
					}
					for (Literal lit : n) {
						System.out.println(lit.getL() + " = " + lit.getAsignacion());
					}
					System.out.println("Formula resuelta");
					System.out.println("El valor booleano final de esta formula es " + f.applyValuesWithNull(n)
							+ ", porque es satisfactible");
				} catch (SATSolverException e) {
					System.out.println("Esta formula no es satisfactible");
				}
			}
		} else {
			System.out.println("No hay formula o no se cargo correctamente");
		}
	}

	/**
	 * Metodo que resuelve la FormulaCNF de esta clase sin imprimir por pantalla
	 */
	private static void resolverFormulaSilent() {
		if (f != null) {
			if (f.is2SAT()) {
				try {
					ArrayList<Literal> n = Algoritmos.LimitedBacktracking(f, new ArrayList<Literal>());
					if (n == null) {
						throw new SATSolverException();
					}
					if (!f.applyValuesWithNull(n)) {
						throw new SATSolverException("Error al resolver");
					}
				} catch (SATSolverException e) {
					if (e.getMessage() != null && e.getMessage().equals("Error al resolver")) {
						e.printStackTrace();
					}
				}
			} else if (f.isHornSAT()) {
				try {
					ArrayList<Literal> n = Algoritmos.Unit(f);
					if (n == null) {
						throw new SATSolverException();
					}
					if (!f.applyValuesWithNull(n)) {
						throw new SATSolverException("Error al resolver");
					}
				} catch (SATSolverException e) {
					if (e.getMessage() != null && e.getMessage().equals("Error al resolver")) {
						e.printStackTrace();
					}
				}
			} else {
				try {
					ArrayList<Literal> n = Algoritmos.DPLL(f, new ArrayList<Literal>());
					if (n == null) {
						throw new SATSolverException();
					}
					if (!f.applyValuesWithNull(n)) {
						throw new SATSolverException("Error al resolver");
					}
				} catch (SATSolverException e) {
					if (e.getMessage() != null && e.getMessage().equals("Error al resolver")) {
						e.printStackTrace();
					}
				}
			}
		} else {
			System.out.println("No hay formula o no se cargo correctamente");
		}
	}

	/**
	 * Metodo que muestra el tipo de la FormulaCNF de esta clase
	 */
	private static void mostrarTipoFormula() {
		if (f != null) {
			System.out.println("La siguiente formula:");
			System.out.println(f.toString());
			if (f.is2SAT()) {
				System.out.println("Es de tipo 2-SAT");
			} else if (f.isHornSAT()) {
				System.out.println("Es de tipo Horn SAT");
			} else {
				System.out.println("Es una formula normal SAT");
			}
		} else {
			System.out.println("No hay formula o no se cargo correctamente");
		}
	}

	/**
	 * Genera una formula CNF de forma aleatoria
	 * 
	 * @param clausulas
	 *            : numero de clausulas de la formula
	 * @param literales
	 *            : numero de literales de la formula
	 * @param tipo
	 *            : tipo de la formula
	 * 
	 * @return FormulaCNF generada aleatoriamente
	 */
	private static FormulaCNF randomFormula(int clausulas, int literales, int tipo) {
		ArrayList<Literal> literals = new ArrayList<Literal>();
		for (int i = 0; i < literales; i++) {
			Literal lit = new Literal("a" + i, true);
			literals.add(lit);
		}

		// Random formula 2SAT
		if (tipo == 0) {
			return new FormulaCNF(randomClausulas2SAT(clausulas, literals));
		} else if (tipo == 1) {
			return new FormulaCNF(randomClausulasHornSAT(clausulas, literals));
		} else {
			return new FormulaCNF(randomClausulasSAT(clausulas, literals));
		}
	}

	/**
	 * Genera un ArrayList de Clausula aleatoriamente. Las clausulas seran todas
	 * 2SAT
	 * 
	 * @param clausulas
	 *            : numero de clausulas
	 * @param literals
	 *            : ArrayList con los literales de las clausulas
	 * 
	 * @return ArrayList con las clausulas 2SAT generadas
	 */
	private static ArrayList<Clausula> randomClausulas2SAT(int clausulas, ArrayList<Literal> literals) {
		Random r = new Random();
		ArrayList<Clausula> c = new ArrayList<Clausula>();

		for (int i = 0; i < clausulas; i++) {
			Literal a = new Literal(literals.get(r.nextInt(literals.size())).getL(), r.nextBoolean());
			Literal b = new Literal(literals.get(r.nextInt(literals.size())).getL(), r.nextBoolean());
			Clausula temp = new Clausula(a, b);
			c.add(temp);
		}

		return c;
	}

	/**
	 * Genera un ArrayList de Clausula aleatoriamente. Las clausulas seran todas
	 * HornSAT
	 * 
	 * @param clausulas
	 *            : numero de clausulas
	 * @param literals
	 *            : ArrayList con los literales de las clausulas
	 * 
	 * @return ArrayList con las clausulas HornSAT generadas
	 */
	private static ArrayList<Clausula> randomClausulasHornSAT(int clausulas, ArrayList<Literal> literals) {
		Random r = new Random();
		ArrayList<Clausula> c = new ArrayList<Clausula>();

		for (int i = 0; i < clausulas; i++) {
			Clausula temp = new Clausula();
			int cuentaPositivos = 0;
			for (Literal lit : literals) {
				// Metemos este literal
				if (r.nextBoolean()) {
					// Lo metemos afirmado si true
					if (r.nextBoolean() && cuentaPositivos == 0) {
						cuentaPositivos++;
						temp.addLiteral(new Literal(lit.getL(), true));
					} else {
						temp.addLiteral(new Literal(lit.getL(), false));
					}
				}
			}
			c.add(temp);
		}

		return c;
	}

	/**
	 * Genera un ArrayList de Clausula aleatoriamente. Las clausulas seran todas
	 * SAT
	 * 
	 * @param clausulas
	 *            : numero de clausulas
	 * @param literals
	 *            : ArrayList con los literales de las clausulas
	 * 
	 * @return ArrayList con las clausulas SAT generadas
	 */
	private static ArrayList<Clausula> randomClausulasSAT(int clausulas, ArrayList<Literal> literals) {
		Random r = new Random();
		ArrayList<Clausula> c = new ArrayList<Clausula>();

		for (int i = 0; i < clausulas; i++) {
			Clausula temp = new Clausula();
			for (Literal lit : literals) {
				// Metemos este literal
				if (r.nextBoolean()) {
					temp.addLiteral(new Literal(lit.getL(), r.nextBoolean()));
				}
			}
			c.add(temp);
		}

		return c;
	}
}
