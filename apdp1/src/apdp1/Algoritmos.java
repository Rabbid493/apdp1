package apdp1;

import java.util.ArrayList;

/**
 * 
 * @author Jaime Ruiz-Borau Vizarraga (NIP: 546751)
 *
 *         En esta clase se implementan los distintos algoritmos para resolver
 *         problemas SAT planteados en el enunciado de la practica 1 de
 *         Algoritmia Para Problemas Dificiles
 */

public class Algoritmos {

	/**
	 * Metodo que aplica el metodo Limited Backtracking para formulas 2SAT
	 * 
	 * @param f
	 *            : FormulaCNF
	 * 
	 * @param assignedSoFar
	 *            : ArrayList de Literales con las asignaciones hasta ahora
	 * 
	 * @return ArrayList de Literales con las asignaciones que hacen true la
	 *         formula
	 * 
	 * @throws SATSolverException
	 *             : Si la formula es no satisfactible
	 */
	public static ArrayList<Literal> LimitedBacktracking(FormulaCNF f, ArrayList<Literal> assignedSoFar)
			throws SATSolverException
			{
		if (f.is2SAT()) {
			FormulaCNF actual = new FormulaCNF(f.getClauses());

			/*
			 * Buscamos clausulas unitarias
			 */
			boolean hayUnitarias = false;
			Clausula found = actual.hasUnitaryClauses();
			while (found != null) {
				/* Convertirla en 2SAT estricto */
				found.addLiteral(found.getLiterals().get(0));
				found = actual.hasUnitaryClauses();
				hayUnitarias = true;
			}

			/*
			 * Si todos los literales tienen el mismo signo en todas sus
			 * ocurrencias se dice que es consistente
			 */
			if (!actual.checkConsistencia2SAT()) {
				throw new SATSolverException("Esta formula no es consistente");
			}

			actual = new FormulaCNF(f.getClauses());

			/*
			 * Buscamos clausulas unitarias para aplicar unit propagation
			 */
			if (hayUnitarias) {
				found = actual.hasUnitaryClauses();
				while (found != null) {
					/* Aplicar unit propagation */
					Literal li = found.getLiterals().get(0);
					assignedSoFar.add(li);
					asignarValor(assignedSoFar, li, !li.isNegated());
					actual.applyUnitPropagation(li);
					found = actual.hasUnitaryClauses();
				}
			}
			/*
			 * Si con las asignaciones hasta el momento alguna clausula se hace
			 * false, no hay solucion, devolvemos null
			 */
			if (actual.checkFalseClauses(assignedSoFar)) {
				return null;
			}

			ArrayList<Literal> literals = actual.getLiterals();

			/*
			 * Elegimos un literal cualquiera para asignar si queda y devolvemos
			 * la asignacion resultante
			 */
			if (actual.getClauses().size() > 0) {
				literals = actual.getLiterals();

				// Cogemos un literal aleatoriamente y lo asignamos
				Literal li = literals.get(0);
				ArrayList<Literal> assignedSoFarRama1 = new ArrayList<Literal>(assignedSoFar);
				assignedSoFarRama1.add(li);
				asignarValor(assignedSoFarRama1, li, true);
				FormulaCNF rama1 = new FormulaCNF(actual.getClauses());
				ArrayList<Literal> resultRama1;
				try {
					rama1.applyArbitraryAssignation(li, true);
					resultRama1 = LimitedBacktracking(rama1, assignedSoFarRama1);
				} catch (SATSolverException e) {
					resultRama1 = null;
				}

				if (resultRama1 == null) {
					ArrayList<Literal> assignedSoFarRama2 = new ArrayList<Literal>(assignedSoFar);
					assignedSoFarRama2.add(li);
					asignarValor(assignedSoFarRama2, li, false);
					FormulaCNF rama2 = new FormulaCNF(actual.getClauses());
					ArrayList<Literal> resultRama2;
					try{
						rama2.applyArbitraryAssignation(li, false);
						resultRama2 = LimitedBacktracking(rama2, assignedSoFarRama2);
					} catch(SATSolverException e){
						resultRama2 = null;
					}

					return resultRama2;
				} else {
					return resultRama1;
				}

			} else {
				return assignedSoFar;
			}
		} else {
			throw new SATSolverException("No es una formula 2SAT");
		}
	}

	/**
	 * Metodo que aplica el metodo de propagacion unitaria para formulas HornSAT
	 * 
	 * @param f
	 *            : FormulaCNF tipo HornSAT
	 * 
	 * @return ArrayList de Literales con las asignaciones que hacen true la
	 *         formula
	 * 
	 * @throws SATSolverException
	 *             : Si la formula es no satisfactible
	 */
	public static ArrayList<Literal> Unit(FormulaCNF f) throws SATSolverException {

		if (f.isHornSAT()) {
			/* 1. Buscar clausulas unitarias */
			ArrayList<Literal> literals = f.getLiterals();

			Clausula found = f.hasUnitaryClauses();
			while (found != null) {
				/* 2. Aplicar unit propagation */
				asignarValor(literals, found.getLiterals().get(0), !found.getLiterals().get(0).isNegated());
				f.applyUnitPropagation(found.getLiterals().get(0));
				found = f.hasUnitaryClauses();
			}

			/*
			 * 3. Si ya no quedan clausulas unitarias asignar a false el resto
			 */
			for (Literal lit : literals) {
				if (lit.getAsignacion() == null) {
					asignarValor(literals, lit, false);
				}
			}

			/*
			 * 4. Damos la asignacion de las variables que hace verdadera la
			 * formula
			 */
			return literals;
		} else {
			throw new SATSolverException("La formula no es HornSAT");
		}
	}

	/**
	 * Metodo que aplica el metodo DPLL para formulas SAT generales
	 * 
	 * @param f
	 *            : FormulaCNF
	 * 
	 * @return ArrayList de Literales con las asignaciones que hacen true la
	 *         formula
	 * 
	 * @throws SATSolverException
	 *             : Si la formula es no satisfactible
	 */
	public static ArrayList<Literal> DPLL(FormulaCNF f, ArrayList<Literal> assignedSoFar) throws SATSolverException {
		if (f.getClauses().size() > 0) {
			FormulaCNF actual = new FormulaCNF(f.getClauses());

			/*
			 * Si todos los literales tienen el mismo signo en todas sus
			 * ocurrencias se dice que es consistente
			 */
			if (actual.checkConsistencia()) {
				assignedSoFar.addAll(actual.solucionTrivial());
				return assignedSoFar;
			}

			/*
			 * Si con las asignaciones hasta el momento alguna clausula se hace
			 * false, no hay solucion, devolvemos null
			 */
			if (actual.checkFalseClauses(assignedSoFar)) {
				return null;
			}

			/*
			 * Buscamos clausulas unitarias
			 */
			Clausula found = actual.hasUnitaryClauses();
			while (found != null) {
				/* Aplicar unit propagation */
				Literal li = found.getLiterals().get(0);
				assignedSoFar.add(li);
				asignarValor(assignedSoFar, li, !li.isNegated());
				actual.applyUnitPropagation(li);
				found = actual.hasUnitaryClauses();
			}

			/*
			 * Buscamos algun literal puro
			 */
			ArrayList<Literal> literals = actual.getLiterals();
			ArrayList<Literal> allLiterals = actual.getAllLiterals();
			for (Literal lit : literals) {
				boolean trueFound = false;
				boolean falseFound = false;
				for (Literal lit2 : allLiterals) {
					if (lit.getL().equals(lit2.getL())) {
						if (!trueFound && !lit2.isNegated()) {
							trueFound = true;
						}
						if (!falseFound && lit2.isNegated()) {
							falseFound = true;
						}
					}
				}
				if (!trueFound && falseFound) {
					assignedSoFar.add(lit);
					asignarValor(assignedSoFar, lit, false);
					actual.applyPureLiteralAssign(new Literal(lit.getL(), false));
				}

				if (trueFound && !falseFound) {
					assignedSoFar.add(lit);
					asignarValor(assignedSoFar, lit, true);
					actual.applyPureLiteralAssign(new Literal(lit.getL(), true));
				}
			}

			/*
			 * Elegimos un literal cualquiera para asignar si queda y devolvemos
			 * la asignacion resultante
			 */
			if (actual.getClauses().size() > 0) {
				literals = actual.getLiterals();

				// Cogemos un literal aleatoriamente y lo asignamos
				Literal li = literals.get(0);
				ArrayList<Literal> assignedSoFarRama1 = new ArrayList<Literal>(assignedSoFar);
				assignedSoFarRama1.add(li);
				asignarValor(assignedSoFarRama1, li, true);
				FormulaCNF rama1 = new FormulaCNF(actual.getClauses());
				rama1.applyArbitraryAssignation(li, true);
				ArrayList<Literal> resultRama1;
				try {
					rama1.applyArbitraryAssignation(li, true);
					resultRama1 = DPLL(rama1, assignedSoFarRama1);
				} catch (SATSolverException e) {
					resultRama1 = null;
				}

				if (resultRama1 == null) {
					ArrayList<Literal> assignedSoFarRama2 = new ArrayList<Literal>(assignedSoFar);
					assignedSoFarRama2.add(li);
					asignarValor(assignedSoFarRama2, li, false);
					FormulaCNF rama2 = new FormulaCNF(actual.getClauses());
					ArrayList<Literal> resultRama2;
					try{
						rama2.applyArbitraryAssignation(li, false);
						resultRama2 = DPLL(rama2, assignedSoFarRama2);
					} catch(SATSolverException e){
						resultRama2 = null;
					}

					return resultRama2;
				} else {
					return resultRama1;
				}

			} else {
				return assignedSoFar;
			}
		} else {
			return assignedSoFar;
		}
	}

	/**
	 * Dada un ArrayList de Literales asigna el valor "value" al Literal "lit"
	 * 
	 * @param list
	 *            : ArrayList de Literales
	 * @param lit
	 *            : Literal a asignar
	 * @param value
	 *            : Valor a asignar
	 */
	private static void asignarValor(ArrayList<Literal> list, Literal lit, boolean value) {
		boolean found = false;
		for (int i = 0; i < list.size() && !found; i++) {
			if (list.get(i).getL().equals(lit.getL())) {
				list.get(i).setAsignacion(value);
				found = true;
			}
		}
	}
}
