package apdp1;

import java.util.ArrayList;

/**
 * 
 * @author Jaime Ruiz-Borau Vizarraga (NIP: 546751)
 *
 *         Esta clase refleja la implementacion de una formula booleana CNF.
 *         Contiene constructores y metodos para acceder a las distintas
 *         componentes de una formula CNF, asi como metodos para aplicar los
 *         algoritmos que se han implementado para resolver los distintos casos
 *         de SAT
 */

public class FormulaCNF {

	/* Atributos privados */

	// ArrayList de Clausulas. Contiene todos las Clausulas de esta FormulaCNF
	private ArrayList<Clausula> l;

	/**
	 * Constructor por defecto
	 */
	public FormulaCNF() {
		l = new ArrayList<Clausula>();
	}

	/**
	 * Constructor
	 * 
	 * @param c
	 *            : Clausula con la que construir una FormulaCNF
	 */
	public FormulaCNF(Clausula c) {
		l = new ArrayList<Clausula>();
		l.add(c);
	}

	/**
	 * Constructor
	 * 
	 * @param list
	 *            : ArrayList que contiene Clausulas con las que construir una
	 *            FormulaCNF
	 */
	public FormulaCNF(ArrayList<Clausula> list) {
		this.l = new ArrayList<Clausula>();
		for (Clausula c : list) {
			this.l.add(new Clausula(c));
		}
	}

	/**
	 * Introduce una Clausula en la lista de Clausulas de esta FormulaCNF
	 * 
	 * @param c
	 *            : Clausula a introducir
	 */
	public void addClausula(Clausula c) {
		l.add(c);
	}

	/**
	 * Introduce las Clausulas de la ArrayList que se le pasa por parametro
	 * 
	 * @param c
	 *            : ArrayList con las Clausulas que se introduciran en la
	 *            FormulaCNF
	 */
	public void addClausulas(ArrayList<Clausula> c) {
		l.addAll(c);
	}

	/**
	 * Metodo que devuelve la lista de Clausulas de esta FormulaCNF
	 * 
	 * @return ArrayList con las Clausulas de esta FormulaCNF
	 */
	public ArrayList<Clausula> getClauses() {
		return l;
	}

	/**
	 * Dada una ArrayList de valores de Literales, los intenta aplicar a cada
	 * una de las Clausulas de esta FormulaCNF y devuelve el valor booleano de
	 * esta FormulaCNF.
	 * 
	 * En caso de haber algun Literal sin valor asignado lanzara una excepcion
	 * 
	 * @param values
	 *            : ArrayList con los valores de los Literales
	 * 
	 * @return true o false dependiendo del valor de esta FormulaCNF
	 * 
	 * @throws SATSolverException
	 *             : Si hay algun Literal sin valor asignado
	 */
	public boolean applyValues(ArrayList<Literal> values) throws SATSolverException {
		if (l.size() == 0) {
			return false;
		} else {
			boolean returned = l.get(0).applyValues(values);
			for (int i = 1; i < l.size(); i++) {
				returned = returned && l.get(i).applyValues(values);
			}
			return returned;
		}
	}

	/**
	 * Dada una ArrayList de valores de Literales, los intenta aplicar a cada
	 * una de las Clausulas de esta FormulaCNF y devuelve un objeto Boolean con
	 * el valor booleano de esta Clausula.
	 * 
	 * En caso de haber algun Literal sin valor asignado devolvera null
	 * 
	 * @param values
	 *            : ArrayList con los valores de los Literales
	 * 
	 * @return Boolean con true o false dependiendo del valor de esta Clausula
	 */
	public Boolean applyValues2(ArrayList<Literal> values) {
		if (l.size() == 0) {
			return false;
		} else {
			Boolean returned = l.get(0).applyValues2(values);
			if (returned != null) {
				for (int i = 1; i < l.size() && returned != null; i++) {
					Boolean returned2 = l.get(i).applyValues2(values);
					if (returned != null && returned2 != null) {
						returned = returned && returned2;
					} else {
						returned = null;
					}
				}
			}
			return returned;
		}
	}

	/**
	 * Dada una ArrayList de valores de Literales, los intenta aplicar a cada
	 * una de las Clausulas de esta FormulaCNF y devuelve el valor booleano de
	 * esta FormulaCNF.
	 * 
	 * @param values
	 *            : ArrayList con los valores de los Literales
	 * 
	 * @return true o false dependiendo del valor de esta FormulaCNF
	 */
	public boolean applyValuesWithNull(ArrayList<Literal> values) {
		if (l.size() == 0) {
			return false;
		} else {
			boolean returned = l.get(0).applyValuesWithNull(values);
			for (int i = 0; i < l.size(); i++) {
				returned = returned && l.get(i).applyValuesWithNull(values);
			}
			return returned;
		}
	}

	/**
	 * Devuelve la lista de Literales de esta FormulaCNF. Devuelve una
	 * ocurrencia para cada Literal
	 * 
	 * @return ArrayList con los Literales de la FormulaCNF
	 */
	public ArrayList<Literal> getLiterals() {
		ArrayList<Literal> returned = new ArrayList<Literal>();
		for (Clausula c : l) {
			for (Literal a : c.getLiterals()) {
				boolean add = true;
				for (int i = 0; i < returned.size() && add; i++) {
					if (a.getL().equals(returned.get(i).getL())) {
						add = false;
					}
				}
				if (add) {
					returned.add(a);
				}
			}
		}
		return returned;
	}

	/**
	 * Devuelve una lista con todas y cada una de las ocurrencias de los
	 * Literales de esta FormulaCNF
	 * 
	 * @return ArrayList con todos los Literales
	 */
	public ArrayList<Literal> getAllLiterals() {
		ArrayList<Literal> returned = new ArrayList<Literal>();
		for (Clausula c : l) {
			for (Literal a : c.getLiterals()) {
				returned.add(a);
			}
		}
		return returned;
	}

	/**
	 * Metodo que elimina Clausulas duplicadas en esta FormulaCNF
	 */
	public void removeDoubles() {
		boolean found = false;
		for (int i = 0; i < l.size() && !found; i++) {
			for (int j = i + 1; j < l.size() && !found; j++) {
				if (l.get(i).equals(l.get(j))) {
					found = true;
					l.remove(j);
					removeDoubles();
				}
			}
		}
	}

	/**
	 * Metodo que elimina Clausulas vacias en esta FormulaCNF
	 */
	public void removeVoids() {
		for (int i = 0; i < l.size(); i++) {
			if (l.get(i).getLiterals().size() == 0) {
				l.remove(i);
				i--;
			}
		}
	}

	/**
	 * Devuelve true si y solo si esta FormulaCNF es de tipo 2SAT
	 * 
	 * @return true si la FormulaCNF es 2SAT. false en caso contrario
	 */
	public boolean is2SAT() {
		boolean itIs = true;
		for (Clausula c : l) {
			itIs = itIs && c.is2SAT();
		}
		return itIs;
	}

	public boolean inferencia2SAT() {
		if (!this.is2SAT()) {
			return false;
		} else {
			System.out.println("Before: " + this.toString());
			// Primer bucle para iterar sobre todas las clausulas
			boolean found = false;
			for (int i = 0; i < l.size() && !found; i++) {

				// Segundo bucle para iterar sobre todas las clausulas
				for (int j = i + 1; j < l.size() && !found; j++) {

					// Tercer bucle para iterar sobre todos los literales de
					// la clausula i-esima
					ArrayList<Literal> lits = l.get(i).getLiterals();
					for (int k = 0; k < lits.size() && !found; k++) {

						Literal lit = lits.get(k);
						boolean check = l.get(j).checkIfOppositeLiteralExists(lit);
						if (check) {
							// Si true reducimos
							found = true;
							ArrayList<Literal> ls = new ArrayList<Literal>();
							ls.add(lits.get(1 - k));
							ls.add(l.get(j).getLiterals().get(1 - l.get(j).checkIfOppositeLiteralExistsInteger(lit)));

							Clausula c2s = new Clausula(ls);
							if (c2s.getLiterals().size() == 1) {
								c2s.addLiteral(c2s.getLiterals().get(0));
							}
							l.remove(j);
							l.remove(i);
							l.add(c2s);

						}
					} // Fin tercer bucle lit

				} // Fin segundo bucle j

			} // Fin primer bucle i
			this.removeDoubles();
			System.out.println("After: " + this.toString());
			System.out.println();
			return found;
		}
	}

	/**
	 * Devuelve true si la FormulaCNF es 2SAT y no contiene dos Clausulas que la
	 * convierten en inconsistente, y por tanto, no satisfactible
	 * 
	 * @return true si la FormulaCNF es consistente. false en caso contrario
	 */
	public boolean checkConsistencia2SAT() {
		if (!this.is2SAT()) {
			return false;
		} else {
			boolean consistente = true;
			ArrayList<Literal> temp = new ArrayList<Literal>();
			for (Clausula c : l) {
				Literal tempLit = c.isSpecialClause();
				if (tempLit != null) {
					for (Literal lit : temp) {
						if (lit.isOpposite(tempLit)) {
							consistente = false;
						}
					}
					if (consistente) {
						temp.add(tempLit);
					}
				}
			}
			return consistente;
		}
	}

	/**
	 * Devuelve true si esta FormulaCNF contiene todos sus Literales con el
	 * mismo signo en todas sus ocurrencias
	 * 
	 * @return true si todos los Literales tienen el mismo signo en todas sus
	 *         ocurrencias. false en caso contrario
	 */
	public boolean checkConsistencia() {
		boolean esConsistente = true;
		ArrayList<Literal> literals = this.getLiterals();
		ArrayList<Literal> allLiterals = this.getAllLiterals();

		for (int i = 0; i < literals.size() && esConsistente; i++) {
			Literal actual = literals.get(i);
			for (Literal lit : allLiterals) {
				if (actual.getL().equals(lit.getL()) && actual.isOpposite(lit)) {
					esConsistente = false;
				}
			}
		}

		return esConsistente;
	}

	/**
	 * Devuelve un ArrayList con las asignaciones de los Literales triviales
	 * para esta FormulaCNF, en la que si un Literal aparece afirmado su valor
	 * es true y si aparece negado su valor es false
	 * 
	 * @return ArrayList con las asignaciones descritas
	 */
	public ArrayList<Literal> solucionTrivial() {
		ArrayList<Literal> solucion = this.getLiterals();
		for (Literal lit : solucion) {
			lit.setAsignacion(new Boolean(!lit.isNegated()));
		}
		return solucion;
	}

	/**
	 * Devuelve true si alguna Clausula se vuelve false con la asignacion actual
	 * de Literales
	 * 
	 * @param assign
	 *            : asignaciones de los Literales
	 * 
	 * @return true si alguna Clausula se ha vuelto false. false en caso
	 *         contrario
	 */
	public boolean checkFalseClauses(ArrayList<Literal> assign) {
		if (l.size() == 0) {
			return false;
		} else {
			boolean thereIs = false;
			for (int i = 0; i < l.size() && !thereIs; i++) {
				Boolean result = l.get(i).applyValues2(assign);
				if (result != null) {
					if (!result) {
						thereIs = true;
					}
				}
			}
			return thereIs;
		}
	}

	/**
	 * Devuelve el Literal pasado por parametro si para este Literal existe una
	 * Clausula 2SAT especial (x or x) o (¬x or ¬x)
	 * 
	 * @param lit
	 *            : Literal para el cual buscar una Clausula especial
	 * 
	 * @return El mismo Literal si se encontro la Clausula especial
	 */
	public Literal checkSpecialClauseExists2SAT(Literal lit) {
		if (!this.is2SAT()) {
			return null;
		} else {
			Literal found = null;
			for (int i = 0; i < l.size() && found == null; i++) {
				Literal otherLit = l.get(i).isSpecialClause();
				if (otherLit != null) {
					if (otherLit.getL().equals(lit.getL())) {
						found = otherLit;
					}
				}
			}
			return found;
		}
	}

	/**
	 * Devuelve true si esta FormulaCNF es HornSAT
	 * 
	 * @return true si la FormulaCNF es HornSAT
	 */
	public boolean isHornSAT() {
		boolean itIs = true;
		for (Clausula c : l) {
			itIs = itIs && c.isHornSAT();
		}
		return itIs;
	}

	/**
	 * Si esta FormulaCNF posee alguna Clausula unitaria la devuelve. En caso
	 * contrario, devuelve null
	 * 
	 * @return la Clausula unitaria encontrada. Null en caso de que no haya
	 */
	public Clausula hasUnitaryClauses() {
		boolean found = false;
		int ifound = -1;
		for (int i = 0; i < l.size() && !found; i++) {
			if (l.get(i).isUnitary() && l.get(i).isHornSAT()) {
				found = true;
				ifound = i;
			}
		}
		if (ifound >= 0)
			return l.get(ifound);
		else
			return null;
	}

	/**
	 * Metodo que aplica propagacion unitaria a cada una de las Clausulas de
	 * esta FormulaCNF dado un Literal pasado por parametro. Eliminara Literales
	 * de su ArrayList de Literales segun el metodo de propagacion unitaria
	 * 
	 * @param x
	 *            : El Literal en el que basar la propagacion unitaria
	 * @throws SATSolverException
	 *             : Si una clausula es unitaria y su unico literal es el
	 *             opuesto al pasado por parametro, es una contradiccion, ya que
	 *             la propagacion unitaria se basa en clausulas unitarias.
	 * 
	 *             Tener al mismo tiempo una clausula unitaria con un signo y
	 *             otra clausula unitaria de signo contrario hace la formula no
	 *             satisfactible
	 */
	public void applyUnitPropagation(Literal x) throws SATSolverException {
		for (int i = 0; i < l.size(); i++) {
			l.get(i).applyUnitPropagation(x);
		}
		removeVoids();
	}

	/**
	 * Metodo que aplica la propagacion de un Literal puro. Esto significa que
	 * hemos encontrado un Literal puro en nuestra formula y procedemos a
	 * asignarle un valor para que convierta en true todas las clausulas en las
	 * que se encuentra.
	 * 
	 * Esto hace que si la Clausula a la que se aplica tiene el Literal en
	 * cuestion, esta Clausula debe ser vaciada de Literales (ya no importan)
	 * para ser eliminada posteriormente de la formula
	 * 
	 * 
	 * @param x
	 *            : El Literal en el que basar la propagacion de Literal puro
	 */
	public void applyPureLiteralAssign(Literal x) {
		for (int i = 0; i < l.size(); i++) {
			l.get(i).applyPureLiteralAssign(x);
		}
		removeVoids();
	}

	/**
	 * Metodo que aplica la propagacion del valor de un Literal. Esto significa
	 * que hemos asignado el valor de un Literal y tenemos que eliminar las
	 * Clausulas que contengan el Literal y haga la Clausula verdadera, o, en
	 * caso de que el Literal al volverse falso no influya en la Clausula,
	 * eliminar dicho Literal de la Clausula.
	 * 
	 * Si la Clausula es unitaria y se hace false por el efecto de la
	 * asignacion, salta una Exception
	 * 
	 * @param x
	 *            : El Literal en el que basar la asignacion
	 * 
	 * @throws SATSolverException
	 *             : Si una clausula es unitaria y su unico literal se hace
	 *             false al hacer la asignacion, esto significa que la Clausula
	 *             es no satisfactible, asi que lanzara una Exception
	 */
	public void applyArbitraryAssignation(Literal x, boolean value) throws SATSolverException {
		for (int i = 0; i < l.size(); i++) {
			l.get(i).applyArbitraryAssignation(x, value);
		}
		removeVoids();
	}

	/**
	 * El metodo toString reemplaza al existente en la clase Object para mostrar
	 * una version mas "familiar" de este objeto de tipo FormulaCNF
	 */
	@Override
	public String toString() {
		String returned = "";
		for (int i = 0; i < l.size(); i++) {
			returned = returned + l.get(i).toString();
			if (i != l.size() - 1) {
				returned = returned + " ∧ ";
			}
		}
		return returned;
	}
}
