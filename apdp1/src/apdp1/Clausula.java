package apdp1;

import java.util.ArrayList;

/**
 * 
 * @author Jaime Ruiz-Borau Vizarraga (NIP: 546751)
 *
 *         Esta clase refleja la implementacion de una Clausula una formula
 *         booleana CNF. Contiene constructores y metodos para acceder a las
 *         distintas componentes de una Clausula, asi como metodos para aplicar
 *         los algoritmos que se han implementado para resolver los distintos
 *         casos de SAT
 */

public class Clausula {

	/* Atributos privados */

	// ArrayList de Literales. Contiene todos los Literales de esta Clausula
	private ArrayList<Literal> l;

	/**
	 * Constructor por defecto
	 */
	public Clausula() {
		l = new ArrayList<Literal>();
	}

	/**
	 * Constructor
	 * 
	 * @param A
	 *            : Un Literal con el que formar esta Clausula
	 */
	public Clausula(Literal A) {
		l = new ArrayList<Literal>();
		l.add(A);
	}

	/**
	 * Constructor
	 * 
	 * @param A
	 *            : Un Literal con el que formar esta Clausula
	 * @param B
	 *            : Otro Literal con el que formar la Clausula
	 */
	public Clausula(Literal A, Literal B) {
		l = new ArrayList<Literal>();
		l.add(A);
		l.add(B);
	}

	/**
	 * Constructor
	 * 
	 * @param c
	 *            : Una Clausula de la que copiar sus atributos privados
	 */
	public Clausula(Clausula c) {
		this.l = new ArrayList<Literal>();
		for (Literal a : c.getLiterals()) {
			this.l.add(new Literal(a));
		}
	}

	/**
	 * Constructor
	 * 
	 * @param list
	 *            : ArrayList de Literales con el que formar esta Clausula
	 */
	public Clausula(ArrayList<Literal> list) {
		this.l = new ArrayList<Literal>();
		for (Literal c : list) {
			this.l.add(new Literal(c));
		}
	}

	/**
	 * Devuelve los Literales de esta Clausula en forma de ArrayList
	 * 
	 * @return ArrayList con todos los Literales de la Clausula
	 */
	public ArrayList<Literal> getLiterals() {
		return l;
	}

	/**
	 * Introduce otro Literal en la ArrayList de Literales de esta Clausula
	 * 
	 * @param a
	 *            : Un Literal que se introducira en la ArrayList
	 */
	public void addLiteral(Literal a) {
		l.add(a);
	}

	/**
	 * Introduce todos los Literales de la ArrayList pasada por parametro
	 * 
	 * @param l2
	 *            : ArrayList cuyos Literales se introduciran en esta Clausula
	 */
	public void addLiterals(ArrayList<Literal> l2) {
		l.addAll(l2);
	}

	/**
	 * Devuelve true si en esta Clausula existe un Literal opuesto al que se le
	 * pasa por parametro. Un Literal es opuesto a otro si ambos tienen el mismo
	 * identificador pero distinto signo
	 * 
	 * @param x
	 *            : El Literal con el que comparar
	 * 
	 * @return true si existe un Literal opuesto. false en caso contrario
	 */
	public boolean checkIfOppositeLiteralExists(Literal x) {
		boolean found = false;
		for (int i = 0; i < l.size() && !found; i++) {
			// Comprobar si algun literal es igual al pasado por parametro pero
			// opuesto
			if (x.isOpposite(l.get(i))) {
				found = true;
			}
		}
		return found;
	}

	/**
	 * Devuelve un entero mayor o igual a cero si en esta Clausula existe un
	 * Literal opuesto al que se le pasa por parametro. Dicho entero representa
	 * la posicion del Literal opuesto en la ArrayList de Literales de esta
	 * Clausula.
	 * 
	 * Un Literal es opuesto a otro si ambos tienen el mismo identificador pero
	 * distinto signo
	 * 
	 * @param x
	 *            : El Literal con el que comparar
	 * 
	 * @return >=0 si existe un Literal opuesto. -1 en caso contrario
	 */
	public int checkIfOppositeLiteralExistsInteger(Literal x) {
		int found = -1;
		for (int i = 0; i < l.size() && found < 0; i++) {
			// Comprobar si algun literal es igual al pasado por parametro pero
			// opuesto
			if (x.isOpposite(l.get(i))) {
				found = i;
			}
		}
		return found;
	}

	/**
	 * Devuelve true si en esta Clausula existe un Literal igual al que se le
	 * pasa por parametro
	 * 
	 * @param x
	 *            : El Literal con el que comparar
	 * 
	 * @return true si existe un Literal igual. false en caso contrario
	 */
	public boolean checkIfLiteralExists(Literal x) {
		boolean found = false;
		for (int i = 0; i < l.size() && !found; i++) {
			// Comprobar si algun literal es igual al pasado por parametro
			if (x.isEqual(l.get(i))) {
				found = true;
			}
		}
		return found;
	}

	/**
	 * Devuelve un entero mayor o igual a cero si en esta Clausula existe un
	 * Literal igual al que se le pasa por parametro. Dicho entero representa la
	 * posicion del Literal igual en la ArrayList de Literales de esta Clausula.
	 * 
	 * @param x
	 *            : El Literal con el que comparar
	 * 
	 * @return >=0 si existe un Literal igual. -1 en caso contrario
	 */
	public int checkIfLiteralExistsInteger(Literal x) {
		int found = -1;
		for (int i = 0; i < l.size() && found < 0; i++) {
			// Comprobar si algun literal es igual al pasado por parametro
			if (x.isEqual(l.get(i))) {
				found = i;
			}
		}
		return found;
	}

	/**
	 * Devuelve true si esta Clausula es igual a la que se le pasa por parametro
	 * 
	 * @param c
	 *            : Clausula con la que comparar
	 * 
	 * @return true si las Clausulas son iguales. false en caso contrario
	 */
	public boolean equals(Clausula c) {
		return equals(new Clausula(c), new Clausula(this));
	}

	/**
	 * Metodo recursivo auxiliar para equals(Clausula c). Misma funcionalidad
	 * que equals(Clausula c)
	 * 
	 * @param c
	 *            : Clausula con la que comparar
	 * @param d
	 *            : Otra Clausula con la que comparar
	 * 
	 * @return true si las clausulas son iguales. false en caso contrario
	 */
	private boolean equals(Clausula c, Clausula d) {
		if (c.getLiterals().size() != d.getLiterals().size()) {
			return false;
		} else if (c.getLiterals().size() == 0) {
			return true;
		} else {
			int position = c.checkIfLiteralExistsInteger(d.getLiterals().get(0));
			if (position >= 0) {
				c.getLiterals().remove(position);
				d.getLiterals().remove(0);
				return equals(c, d);
			} else {
				return false;
			}
		}
	}

	/**
	 * Dada una ArrayList de valores de Literales, los intenta aplicar a cada
	 * uno de los Literales de esta Clausula y devuelve el valor booleano de
	 * esta Clausula.
	 * 
	 * En caso de haber algun Literal sin valor asignado lanzara una excepcion
	 * 
	 * @param values
	 *            : ArrayList con los valores de los Literales
	 * 
	 * @return true o false dependiendo del valor de esta Clausula
	 * 
	 * @throws SATSolverException
	 *             : Si hay algun Literal sin valor asignado
	 */
	public boolean applyValues(ArrayList<Literal> values) throws SATSolverException {
		if (l.size() == 0) {
			return true;
		} else {
			boolean returned = l.get(0).applyValues(values);
			for (int i = 1; i < l.size(); i++) {
				returned = returned || l.get(i).applyValues(values);
			}
			return returned;
		}
	}

	/**
	 * Dada una ArrayList de valores de Literales, los intenta aplicar a cada
	 * uno de los Literales de esta Clausula y devuelve un objeto Boolean con el
	 * valor booleano de esta Clausula.
	 * 
	 * En caso de haber algun Literal sin valor asignado devolvera null
	 * 
	 * @param values
	 *            : ArrayList con los valores de los Literales
	 * 
	 * @return Boolean con true o false dependiendo del valor de esta Clausula
	 */
	public Boolean applyValues2(ArrayList<Literal> values) {
		if (l.size() == 0) {
			return true;
		} else {
			Boolean returned;
			try {
				returned = l.get(0).applyValues(values);
				for (int i = 1; i < l.size(); i++) {
					returned = returned || l.get(i).applyValues(values);
				}
			} catch (SATSolverException e) {
				returned = null;
			}
			return returned;
		}
	}

	/**
	 * Dada una ArrayList de valores de Literales, los intenta aplicar a cada
	 * uno de los Literales de esta Clausula y devuelve el valor booleano de
	 * esta Clausula.
	 * 
	 * En caso de haber algun Literal sin valor asignado hace un OR con false
	 * 
	 * @param values
	 *            : ArrayList con los valores de los Literales
	 * 
	 * @return true o false dependiendo del valor de esta Clausula
	 */
	public boolean applyValuesWithNull(ArrayList<Literal> values) {
		if (l.size() == 0) {
			return true;
		} else {
			boolean returned = false;
			for (int i = 0; i < l.size(); i++) {
				try {
					returned = returned || l.get(i).applyValues(values);
				} catch (SATSolverException e) {
					returned = returned || false;
				}
			}
			return returned;
		}
	}

	/**
	 * Devuelve true si y solo si esta Clausula es de tipo 2SAT
	 * 
	 * @return true si la Clausula es 2SAT. false en caso contrario
	 */
	public boolean is2SAT() {
		return l.size() <= 2;
	}

	/**
	 * Devuelve true si y solo si esta Clausula es de tipo HornSAT
	 * 
	 * @return true si la Clausula es HornSAT. false en caso contrario
	 */
	public boolean isHornSAT() {
		int cuentaPositivos = 0;
		for (int i = 0; i < l.size() && cuentaPositivos < 2; i++) {
			if (!l.get(i).isNegated()) {
				cuentaPositivos++;
			}
		}
		return (cuentaPositivos < 2);
	}

	/**
	 * Devuelve true si y solo si esta Clausula es unitaria
	 * 
	 * @return true si la Clausula es unitaria. false en caso contrario
	 */
	public boolean isUnitary() {
		return l.size() == 1;
	}

	/**
	 * Metodo que aplica propagacion unitaria a la Clausula dado un Literal
	 * pasado por parametro. Eliminara Literales de su ArrayList de Literales
	 * segun el metodo de propagacion unitaria
	 * 
	 * @param x
	 *            : El Literal en el que basar la propagacion unitaria
	 * @throws SATSolverException
	 *             : Si una clausula es unitaria y su unico literal es el
	 *             opuesto al pasado por parametro, es una contradiccion, ya que
	 *             la propagacion unitaria se basa en clausulas unitarias.
	 * 
	 *             Tener al mismo tiempo una clausula unitaria con un signo y
	 *             otra clausula unitaria de signo contrario hace la formula no
	 *             satisfactible
	 */
	public void applyUnitPropagation(Literal x) throws SATSolverException {
		for (int i = 0; i < l.size(); i++) {
			if (l.get(i).isEqual(x)) {
				this.l = new ArrayList<Literal>();
			} else if (l.get(i).isOpposite(x) && l.size() > 1) {
				l.remove(i);
				i--;
			} else if (l.get(i).isOpposite(x) && l.size() == 1) {
				throw new SATSolverException("Contradiccion encontrada. Esta formula no es satisfactible");
			}
		}
	}

	/**
	 * Metodo que aplica la propagacion de un Literal puro. Esto significa que
	 * hemos encontrado un Literal puro en nuestra formula y procedemos a
	 * asignarle un valor para que convierta en true todas las clausulas en las
	 * que se encuentra.
	 * 
	 * Esto hace que si la Clausula tiene el Literal en cuestion, esta Clausula
	 * debe ser vaciada de Literales (ya no importan) para ser eliminada
	 * posteriormente de la formula
	 * 
	 * 
	 * @param x
	 *            : El Literal en el que basar la propagacion de Literal puro
	 */
	public void applyPureLiteralAssign(Literal x) {
		for (int i = 0; i < l.size(); i++) {
			if (l.get(i).getL().equals(x.getL())) {
				this.l = new ArrayList<Literal>();
			}
		}
	}

	/**
	 * Metodo que aplica la propagacion del valor de un Literal. Esto significa
	 * que hemos asignado el valor de un Literal y tenemos que eliminar las
	 * Clausulas que contengan el Literal y haga la Clausula verdadera, o, en
	 * caso de que el Literal al volverse falso no influya en la Clausula,
	 * eliminar dicho Literal de la Clausula.
	 * 
	 * Si la Clausula es unitaria y se hace false por el efecto de la
	 * asignacion, salta una Exception
	 * 
	 * @param x
	 *            : El Literal en el que basar la asignacion
	 * 
	 * @throws SATSolverException
	 *             : Si una clausula es unitaria y su unico literal se hace
	 *             false al hacer la asignacion, esto significa que la Clausula
	 *             es no satisfactible, asi que lanzara una Exception
	 */
	public void applyArbitraryAssignation(Literal x, boolean value) throws SATSolverException {
		for (int i = 0; i < l.size(); i++) {
			if (l.get(i).getL().equals(x.getL()) && l.get(i).isNegated() && !value) {
				this.l = new ArrayList<Literal>();
			} else if (l.get(i).getL().equals(x.getL()) && !l.get(i).isNegated() && value) {
				this.l = new ArrayList<Literal>();
			} else if (l.get(i).getL().equals(x.getL()) && !l.get(i).isNegated() && !value) {
				if (l.size() > 1) {
					l.remove(i);
					i--;
				} else {
					throw new SATSolverException("Contradiccion encontrada. Esta formula no es satisfactible");
				}
			} else if (l.get(i).getL().equals(x.getL()) && l.get(i).isNegated() && value) {
				if (l.size() > 1) {
					l.remove(i);
					i--;
				} else {
					throw new SATSolverException("Contradiccion encontrada. Esta formula no es satisfactible");
				}
			}
		}
	}

	/**
	 * Devuelve true si esta Clausula es una Clausula del tipo (x or x) o del
	 * tipo (¬x or ¬x)
	 * 
	 * @return true si la Clausula es (x or x) o si es (¬x or ¬x). false en caso
	 *         contrario
	 */
	public Literal isSpecialClause() {
		if (is2SAT()) {
			if (l.get(0).isEqual(l.get(1))) {
				return l.get(0);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * El metodo toString reemplaza al existente en la clase Object para mostrar
	 * una version mas "familiar" de este objeto de tipo Clausula
	 */
	@Override
	public String toString() {
		String returned = "(";
		for (int i = 0; i < l.size(); i++) {
			returned = returned + l.get(i).toString();
			if (i != l.size() - 1) {
				returned = returned + " ∨ ";
			} else {
				returned = returned + ")";
			}
		}
		return returned;
	}
}
