package apdp1;

import java.util.ArrayList;

/**
 * 
 * @author Jaime Ruiz-Borau Vizarraga (NIP: 546751)
 *
 *         Esta clase refleja la implementacion de un Literal de una Clausula en
 *         una formula booleana CNF. Contiene constructores y metodos para
 *         acceder a las distintas componentes de un Literal dentro de una
 *         Clausula
 */

public class Literal {

	/* Atributos privados */

	// Identificador
	private String l;

	// Booleano n. Si es false este Literal esta NEGADO. Si es true esta NORMAL
	private boolean n;

	// Boolean asignacion. Si es true el valor de este literal es TRUE, y si es
	// false sera FALSE
	private Boolean asignacion;

	/**
	 * Constructor
	 * 
	 * @param L
	 *            : Nombre del Literal
	 * @param N
	 *            : Si esta negado o no
	 */
	public Literal(String L, boolean N) {
		l = L;
		n = N;
		asignacion = null;
	}

	/**
	 * Constructor
	 * 
	 * @param l
	 *            : Otro Literal del que copiar sus parametros
	 */
	public Literal(Literal l) {
		this.l = new String(l.getL());
		this.n = l.isNormal();
		if (l.getAsignacion() != null) {
			asignacion = new Boolean(l.getAsignacion());
		} else {
			asignacion = null;
		}
	}

	/**
	 * Devuelve el identificador de este Literal
	 * 
	 * @return el identificador del Literal
	 */
	public String getL() {
		return l;
	}

	/**
	 * Devuelve el valor que ha tomado este Literal
	 * 
	 * @return el valor tomado por el Literal
	 */
	public Boolean getAsignacion() {
		return asignacion;
	}

	/**
	 * Ajusta el valor que tomara este Literal
	 * 
	 * @param b
	 *            : el nuevo valor del Literal
	 */
	public void setAsignacion(boolean b) {
		asignacion = new Boolean(b);
	}

	/**
	 * Devuelve true si este Literal no esta negado
	 * 
	 * @return true si no esta negado. false en caso contrario
	 */
	public boolean isNormal() {
		return n;
	}

	/**
	 * Devuelve true si este Literal esta negado
	 * 
	 * @return true si esta negado. false en caso contrario
	 */
	public boolean isNegated() {
		return !n;
	}

	/**
	 * Devuelve true si este Literal es igual en identificador y negacion a
	 * other
	 * 
	 * @param other
	 *            : el otro Literal con el que comparar
	 * 
	 * @return true si son iguales. false en caso contrario
	 */
	public boolean isEqual(Literal other) {
		if (this.getL().equals(other.getL()) && this.isNegated() == other.isNegated()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Devuelve true si este Literal es igual en identificador al Literal other,
	 * pero tienen distinto signo
	 * 
	 * @param other
	 *            : el otro Literal con el que comparar
	 * 
	 * @return true si son opuestos. false en caso contrario
	 */
	public boolean isOpposite(Literal other) {
		if (this.getL().equals(other.getL())) {
			if (this.isNormal() && other.isNegated()) {
				return true;
			} else if (this.isNegated() && other.isNormal()) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Devuelve true si este Literal es distinto en identificador al Literal
	 * other
	 * 
	 * @param other
	 *            : el otro Literal con el que comparar
	 * 
	 * @return true si son distintos. false en caso contrario
	 */
	public boolean isDifferent(Literal other) {
		return !this.l.equals(other.getL());
	}

	/**
	 * Devuelve el valor que tomara este Literal teniendo en cuenta su signo y
	 * el valor que se le ha asignado. Dicho valor lo busca en la ArrayList de
	 * Literales que se le pasa por parametro, donde se deberia encontrar este
	 * Literal con su Boolean "asignacion" con un valor concreto.
	 * 
	 * En caso de no tener un Boolean "asignacion" (sera null) este metodo
	 * lanzara una Exception para reflejar que a este Literal no se le ha
	 * asignado un valor todavia
	 * 
	 * @param values
	 *            : el objeto ArrayList donde este Literal buscara su valor
	 * 
	 * @return true o false dependiendo del signo del Literal y su valor
	 * 
	 * @throws SATSolverException
	 *             si este Literal no tiene valor asignado
	 */
	public boolean applyValues(ArrayList<Literal> values) throws SATSolverException {
		Boolean miAsignacion = null;
		boolean found = false;
		for (int i = 0; i < values.size() && !found; i++) {
			if (values.get(i).getL().equals(this.getL())) {
				miAsignacion = values.get(i).getAsignacion();
				found = false;
			}
		}
		if (miAsignacion == null) {
			throw new SATSolverException("No hay valor para este literal");
		} else {
			if (this.isNegated())
				return !miAsignacion;
			else
				return miAsignacion;
		}
	}

	/**
	 * El metodo toString reemplaza al existente en la clase Object para mostrar
	 * una version mas "familiar" de este objeto de tipo Literal
	 */
	@Override
	public String toString() {
		if (n) {
			return "" + l;
		} else {
			return "¬" + l;
		}
	}
}
